# Threat Intel Graph and notes

This is a project I did for fun in order to paint a graph and note stuff down based on the relationship between tools and threat actors.

## Getting started

The tool is easy to use, although here it goes some tips on it. 

First of all, for deploying, simply write
```
./launch_w.sh NameIWantForMyGraph
```
You will see a menu. The idea is that, in the end, you will se a grpah you can drag and think around with, in which you can see the relationship between threat actors and their tools. So, from the terminal you will be able to add Threat actors, Tools or connections. Let's try all. First of all, adding a Threat Actor.

```
Choose an option:
1 - Add a Threat Actor
2 - Add a Tool
3 - Add a connection with existing values
4 - Exit
OPTION > 1
```

Now a bunch of data will appear. This is the current state of the graph, with names, nodes and more, in case you want to check. Now, let's add "EvilCorp" for example. 

```
Here are the default nodes: 
[{'color': '#ffcc66', 'id': 2, 'label': 'Wizard Spider', 'shape': 'dot'}, {'color': '#ffcc66', 'id': 4, 'label': 'TA505', 'shape': 'dot'}, {'color': '#ff6666', 'id': 1, 'label': 'TrickBot', 'shape': 'dot'}, {'color': '#33cccc', 'id': 3, 'label': 'BloodHound', 'shape': 'dot'}]
Threat actor name > Evil Corp
adding Evil Corp
Connected to ID N (0-none) > 
```

In case we see a node of a tool (or a threat actor) we want to connect to evil Corp, this is the moment. If you don want to, just write 0.

```
Connected to ID N (0-none) > 0
Only created the node
Add more? Y/N > Y
```
We created an isolated node. Let's add another Threat actor, such as Mustang Panda.

```
Okay
Here are the default nodes: 
[{'color': '#ffcc66', 'id': 2, 'label': 'Wizard Spider', 'shape': 'dot'}, {'color': '#ffcc66', 'id': 4, 'label': 'TA505', 'shape': 'dot'}, {'color': '#ff6666', 'id': 1, 'label': 'TrickBot', 'shape': 'dot'}, {'color': '#33cccc', 'id': 3, 'label': 'BloodHound', 'shape': 'dot'}, {'color': '#ffcc66', 'id': 6, 'label': 'Evil Corp', 'shape': 'dot'}]
Threat actor name > Mustang Panda
adding Mustang Panda
Connected to ID N (0-none) > 0
Only created the node
Add more? Y/N > N
Okay, done here.
```
Now we return to the menu. Let's add a Tool, Cobalt Strike.

```
Choose an option:
1 - Add a Threat Actor
2 - Add a Tool
3 - Add a connection with existing values
4 - Exit
OPTION > 2
Here are the default nodes: 
[{'color': '#ffcc66', 'id': 2, 'label': 'Wizard Spider', 'shape': 'dot'}, {'color': '#ffcc66', 'id': 4, 'label': 'TA505', 'shape': 'dot'}, {'color': '#ff6666', 'id': 1, 'label': 'TrickBot', 'shape': 'dot'}, {'color': '#33cccc', 'id': 3, 'label': 'BloodHound', 'shape': 'dot'}, {'color': '#ffcc66', 'id': 6, 'label': 'Evil Corp', 'shape': 'dot'}, {'color': '#ffcc66', 'id': 8, 'label': 'Mustang Panda', 'shape': 'dot'}]
Tool name > Cobalt Strike
Tool node color (#ref) for example:#33cc33,#ff99ff,#ffff33 > #ff99ff
```

In this case we will also choose a color. The color of the threat actors is always the same by default, but the tools are not (if you want, change that in the code! that's for my own convenience). Now it will ask us to connect it to a node if we want- In this case we want to connect it to Evil Corp. As we can see in the data: `{'color': '#ffcc66', 'id': 6, 'label': 'Evil Corp', 'shape': 'dot'}`, Evil Corp is id, 6.

```
adding Cobalt Strike
Connected to ID N (0-none) > 6
Added connection
Add more? Y/N > N
Okay, done here.
```

Now, we just remembered that we wanted Mustang Panda to be connected to Cobalt Strike as well. No problem, in the menu, let's choose option 3. We will need to know the nodes of Cobalt Strike (`{'color': '#ff99ff', 'id': 5, 'label': 'Cobalt Strike', 'shape': 'dot'}`) and Mustand Panda (`{'color': '#ffcc66', 'id': 8, 'label': 'Mustang Panda', 'shape': 'dot'}`), the order doesn matter. 

```
Choose an option:
1 - Add a Threat Actor
2 - Add a Tool
3 - Add a connection with existing values
4 - Exit
OPTION > 3
Here are the default nodes: 
[{'color': '#ffcc66', 'id': 2, 'label': 'Wizard Spider', 'shape': 'dot'}, {'color': '#ffcc66', 'id': 4, 'label': 'TA505', 'shape': 'dot'}, {'color': '#ff6666', 'id': 1, 'label': 'TrickBot', 'shape': 'dot'}, {'color': '#33cccc', 'id': 3, 'label': 'BloodHound', 'shape': 'dot'}, {'color': '#ffcc66', 'id': 6, 'label': 'Evil Corp', 'shape': 'dot'}, {'color': '#ffcc66', 'id': 8, 'label': 'Mustang Panda', 'shape': 'dot'}, {'color': '#ff99ff', 'id': 5, 'label': 'Cobalt Strike', 'shape': 'dot'}]
Node 1 > 5
Node 2 > 8
Connecting nodes...
Done
Connect more? Y/N > N
Okay, done here.
```

Now we are all set, we want to work on that already. We exit the menu using option 4.

```
Choose an option:
1 - Add a Threat Actor
2 - Add a Tool
3 - Add a connection with existing values
4 - Exit
OPTION > 4
Bye bye!
```

And then in firefox it should appear both the graph alone and a pretty front-end I made for seeing the graph and taking notes.

![](https://gitlab.com/terceranexus6/TIGN/-/raw/main/images/exam.gif)

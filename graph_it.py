from pyvis.network import Network
import sys

net = Network()


def add_ta( threatactor, nID):
    print('adding '+threatactor)
    net.add_nodes([nID],label=[threatactor],color=['#ffcc66'])

def add_tool( tool, nID, color_1):
    print('adding '+tool)
    net.add_nodes([nID],label=[tool],color=[color_1])

def menu():
    print('Choose an option:')
    print('1 - Add a Threat Actor')
    print('2 - Add a Tool')
    print('3 - Add a connection with existing values')
    print('4 - Exit')


# Adding Threat actors
net.add_nodes([2, 4], 
              label=['Wizard Spider', 'TA505'],
              color=['#ffcc66','#ffcc66'])


# Adding Tools
net.add_nodes([1,3],
            label=['TrickBot','BloodHound'],
            color=['#ff6666','#33cccc']
        )

# Counters
cou = 4
cou2 = 3


#Adding the ones connected to TrickBot
net.add_edges([(1,2),(1,4)])

#Adding the ones connected to BloodHound
net.add_edges([(3,2)])

#Choose an output name
file_n = sys.argv[1]
file_n = file_n+'.html'

# MAIN PROGRAM
keep_up = True
while keep_up:
    menu()
    option = input('OPTION > ')

    # option one is adding a threat actor
    if option == '1':

        #starting a loop in case more threat actors are added
        ta_loop = True
        while ta_loop:
            # A quick update on the already added nodes
            print('Here are the default nodes: ')
            print(net.nodes)
            ta = input('Threat actor name > ')
            # here's a counter for the id of the ta, which is pair
            # so the tools could be odd
            cou = cou + 2
            add_ta(ta, cou)
            #You can create a connection to an existing tool (or ta)
            co = input('Connected to ID N (0-none) > ')
            if co == '0':
                #if no connection is created just the node will appear
                print('Only created the node')
            else:
                co = int(co)
                net.add_edge(co, cou, tittle='test')
                print('Added connection')
            #the loop is revised in case you want to add more nodes    
            loop1 = input('Add more? Y/N > ')
            if loop1 == 'N':
                print('Okay, done here.')
                ta_loop = False
            else:
                print('Okay')
    #second option is adding tools, the rest is pretty similar
    elif option == '2':
        tool_loop = True
        while tool_loop:
            # A quick update on the already added nodes
            print('Here are the default nodes: ')
            print(net.nodes)
            tool = input('Tool name > ')
            #tcolor = input('#ffff33')
            tcolor = input('Tool node color (#ref) for example:#33cc33,#ff99ff,#ffff33 > ')
            # here's a counter for the id of the tools, which is odds
            cou2 = cou2 + 2
            add_tool(tool, cou2, tcolor)
            #You can create a connection to an existing tool (or ta)
            co = input('Connected to ID N (0-none) > ')
            if co == '0':
                #if no connection is created just the node will appear
                print('Only created the node')
            else:
                co = int(co)
                net.add_edge(co, cou2)
                print('Added connection')
            #the loop is revised in case you want to add more nodes    
            loop1 = input('Add more? Y/N > ')
            if loop1 == 'N':
                print('Okay, done here.')
                tool_loop = False
            else:
                print('Okay.')

    elif option == '3':
        conn_loop = True
        while conn_loop:
            print('Here are the default nodes: ')
            print(net.nodes)
            no1 = input('Node 1 > ')
            no1 = int(no1)
            no2 = input('Node 2 > ')
            no2 = int(no2)
            print('Connecting nodes...')
            net.add_edge(no1, no2)
            print('Done')
            loop2 = input('Connect more? Y/N > ')
            if loop2 == 'N':
                print('Okay, done here.')
                conn_loop = False
            else:
                print('Okay.')

    elif option == '4':
        print('Bye bye!')
        keep_up = False


#net.repulsion(node_distance=80, spring_length=200)
net.show(file_n)

